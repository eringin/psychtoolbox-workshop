# Getting to grips with MATLAB

![](images/mat-lab.jpg) 

MATLAB (short for MATrix LABoratory) is a programming language created for performing computations on matrices and vectors. 

You may have very bad memories involving the words 'vector' and 'matrix' from your maths or physics classes (think of matrix multiplication done by hand). However matrices and vectors are much easier to deal with when writing code, and they are extremely useful. We will go through some basic types of data that MATLAB handles, and how you can manipulate these using code.

## Numbers, strings and logicals



### Using MATLAB as a calculator




## Vectors

**Vectors** can be defined as a set of elements stored in a single 'container'. Let's say we have a set of three numbers, 1, 2, and 6. We can store these in a single 'variable' or 'vessel' as `[1, 2, 6]`. These might, for example, represent the X, Y and Z coordinates in 3D space. Another way we might use vectors is to store the RGB (Red Green Blue) colour values for a stimulus. For example, [0, 0, 0] would be the colour black and [255, 255, 255] would be the colour white (we will use colours in this way later on).

The square brackets are used in MATLAB to 'stick things together' into one vector or other type of container. This process is also called **concatenation**. You can glue together different types of numbers and words in this way.

More examples:

```
stimulus_position_x_y = [100, 400]

reaction_times_ms = [325, 400, 357, 580, 300]

one_to_twenty = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

red_colour_rgb = [255, 0, 0]

green_colour_rgb = [0, 255, 0]

blue_colour_rgb = [0, 0, 255]

```

You can pull a specific number out of a vector by using **indexing**. Each entry (i.e., number) in the vector has an index which denotes the position of that number. For example, the first entry of the `red_colour_rgb` vector listed above is 255, the second is 0 and the third is 0. The first entry for `stimulus_position_x_y` is 100, and the second entry is 400. 

Often when coding experiments we will only want a single number out a vector to use to present stimuli or determine the conditions in an experimental trial. To do this we can write the vector name with parentheses (round brackets) and the number of the entry we want to use. If you want all the entries you can use `:` or you can get entries 1, 2, 3, ... 10 by typing `1:10`.

```
red_colour_rgb(1) 

% MATLAB Output
= 255

reaction_times_ms(3)

% MATLAB Output
= 400 

reaction_times_ms(:)

% MATLAB Output
= 325, 400, 357, 580, 300

one_to_twenty(1:10)

% MATLAB Output
= 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

```

Words can also be strung together in a single vector, like so:

```
response_hand = 'right'

participant_instructions = ['please respond using your ' response_hand ' index finger']

% MATLAB output:
= 'please respond using your right index finger'


```

## Matrices

**Matrices** are like vectors, except that they do not need to be one single row, but can be a square of numbers or a different shape. Broadly speaking, a vector is a one-dimensional type of matrix (i.e., it only has one coordinate that we can call to get different entries). A matrix can have any number of dimensions, or coordinates of different numbers in the matrix.

The easiest way to understand a matrix is to show a simple 2D example:

```
my_matrix = [1, 2, 3;
			 4, 5, 6;
 			 7, 8, 9]

```

This matrix is a square of numbers 1-9. The first three numbers in the top row are 1-3, the numbers in the second row are 4-6, the numbers in the third row are 7-9. We now need to enter two coordinates in order to access a number in a matrix:

```
my_matrix(1,1) % First row, first column

% MATLAB Output
= 1

my_matrix(3,3) % Third row, third column

% MATLAB Output
= 9

my_matrix(2,3) % Second row, third column

% MATLAB Output
= 6

```

