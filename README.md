# Psychtoolbox Workshop

Welcome to the Decision Neuroscience Lab Psychtoolbox Workshop! This workshop contains material and code to get you coding experiments with Psychtoolbox (and style). It introduces the basics of Psychtoolbox and prepares you to create your own experiments in MATLAB. It also includes a brief introduction to coding in MATLAB. There are exercises and examples provided to help you along the way.

This workshop was created for the Decision Neuroscience Lab Offsite Day at the Abbotsford Convent. However you can complete this workshop without attending this workshop or visiting a convent of any kind. 

All workshop material is stored in the [wiki](https://bitbucket.org/labmembers/psychtoolbox-workshop/wiki/Home) of this repository.

Written by Daniel Feuerriegel, April 2018 at the University of Melbourne. This workshop is covered under a Creative Commons [CC-By Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/). 


## What does this workshop cover?

In this workshop we covers the basics of using Psychtoolbox to code experiments. [Psychtoolbox](http://psychtoolbox.org/overview/) (short for Psychophysics Toolbox) is a collection of functions (or 'mini-programs') which allow you to present visual and auditory stimuli with precise timing. The software architecture of modern computers is generally bad at presenting carefully-controlled stimuli (as required for cognitive experiments). Psychtoolbox was developed as a way to circumvent these limitations and is especially useful for controlling the appearance and timing of stimuli that are presented. We will learn to use the core functions of Psychtoolbox to present our stimuli and design our experiments.

To access the workshop material, go to the [wiki](https://bitbucket.org/labmembers/psychtoolbox-workshop/wiki/Home).

## Things to prepare before attending this workshop

To complete this workshop you will need to have Psychtoolbox 3 and MATLAB installed on your computer. Instructions for installing Psychtoolbox are provided [here](http://psychtoolbox.org/download/). MATLAB licenses are provided by the University of Melbourne (see [the UoM software page](http://unimelb.libguides.com/c.php?g=402784&p=2740903)). We will not have time during the hands-on session to install Psychtoolbox and MATLAB, so make sure you get these before coming to the workshop. 

People often encounter problems when installing Psychtoolbox. Please try installing it early (i.e. a week before the workshop day) so that you can get other lab members to help you if things go wrong.
